from unittest import TestCase
from src.blog import Blog
from read_input import ReadInput

class BlogTest(TestCase):

    def setUp(self):
        blogs = ReadInput().get_blog_input()[0].split(",")
        self.blog = blogs[0].strip()
        self.author = blogs[1].strip()
        self.blog1 = Blog(self.blog, self.author)


    def test_blog_create(self):
        self.assertEqual(self.blog1.title,self.blog)
        self.assertEqual(self.blog1.author,self.author)
        self.assertEqual(0,len(self.blog1.posts))

    def test_repr(self):
        self.assertEqual(self.blog1.__repr__(),f"{self.blog} by {self.author}")