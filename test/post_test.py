from unittest import TestCase
from src.post import Post
from read_input import ReadInput


class PostTest(TestCase):

    def setUp(self):
        posts = ReadInput().get_post_input()[0].split(",")
        self.title = posts[0].strip()
        self.content = posts[1].strip()
        self.post_1 = Post(self.title, self.content)

    def test_create_post(self):
        self.assertEqual(self.post_1.title, self.title)
        self.assertEqual(self.post_1.content, self.content)

    def test_create_post_dict(self):
        expected = {"title":self.title, "content": self.content}
        self.assertDictEqual(expected, self.post_1.dict())